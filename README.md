# Meteor_nginx_docker_compose - A Simple Tool to Run Meteor App with an Nginx Reverse Proxy

Meteor_nginx_docker_compose is meant to help you run your Meteor app with a reverse proxy of Nginx at the front end in docker containers.

## Notice
- Ensure docker compose **v1.6**  or above is installed.

## How to Use


###  1. Build Your Meteor App into a Plain Node Js Application

The Meteor tool has a command `meteor build` that creates a deployment bundle which contains a plain Node.js application.

Before using it, you'll have to install all the npm dependencis for your app in advance.

Also it's important to chose a correct target architecture for your app, you can specify it  with `--architecture`.

Issue these commands inside your Meteor app folder.

~~~shell
npm install --production
meteor build /tmp --architecture os.linux.x86_64
~~~

After finishing these two commands, you will find a bundle file in **/tmp**.(Named as projectName.tar.gz)

###  2. Clone meteor_nginx_docker_compose

~~~shell
cd /tmp
git clone https://gitlab.com/sajidkhan/meteor_nginx_docker_compose.git
cp projectName.tar.gz  meteor_nginx_docker_compose/
cd meteor_nginx_docker_compose/
~~~

### 4. Edit bundlefiles on meteor.dockerfile
~~~shell
FROM meteor/meteor-base
USER 0:0
ENV bundleFile=projectName.tar.gz
USER mt
WORKDIR /home/mt/app
USER 0:0
COPY --chown=mt projectName.tar.gz  .
RUN tar -xf projectName.tar.gz
USER mt
WORKDIR /home/mt/app/bundle/programs/server
RUN npm install
WORKDIR /home/mt/app/bundle
ENTRYPOINT ["node", "main.js"]
~~~

### 4. Build And Run
~~~shell
docker-compose up --build
~~~
You can also run `docker-compose up -d --build` to run your application in the background.

