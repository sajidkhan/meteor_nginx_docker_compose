FROM meteor/meteor-base
USER 0:0
ENV bundleFile=bundle.tar.gz
USER mt
WORKDIR /home/mt/app
USER 0:0
COPY --chown=mt bundle.tar.gz  .
RUN tar -xf bundle.tar.gz
USER mt
WORKDIR /home/mt/app/bundle/programs/server
RUN npm install
WORKDIR /home/mt/app/bundle
ENTRYPOINT ["node", "main.js"]